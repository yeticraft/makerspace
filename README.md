[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/yeticraft/makerspace)

# makerspace

A simple starter Django project that can be used to generate a a site from scratch or to work on the active web enabled application system.

## Create a new site

To start, clone this project onto your system and then switch to the 'newsite' tag like so:

```
git clone git@gitlab.com:yeticraft/makerspace.git
cd makerspace
git checkout newsite
```

This will make sure that you are starting from a fresh set of files, with no design decisions yet made.  

Next, initialize the project files like so:

```
git checkout -b 1.0
APP_NAME=mysite DOCKER_REPO=registry.gitlab.com/myaccount make initialize
```
NOTE: change the values 'mysite' and 'myaccount' to more relevant and real data if you are intending to eventually puplish this application at some point.

This will ask several questions and guide you through the initial phase of creating the various bits automatically.

For a first time testrun, I suggest using 'en' for the language when asked, enter 'America/Los_Angeles' for the timezone, select yes for' Use Bootstrap 4', select 'no for 'reversion support', and defaults for pretty much everythign else.

Now you can continue on with the the commands below as if you are working on Makerspace - but it is an independent site!  Your git space should be initialized and ready for committing, but it will need it's upstream origin to to be set before you can safely store your versions on a remote repository.

Once that is finished, you can continue on with the the commands below as if you are working on Your new site - but it is an independant site!  Your git space should be initialized and ready for commiting, but it will need it's upstream origin to to be set before you can safely store your versions on a remote repository.

# Plugins

* https://django-debug-toolbar.readthedocs.io/en/latest/ - https://gist.github.com/douglasmiranda/9de51aaba14543851ca3
* https://django-crispy-forms.readthedocs.io/en/latest/install.html
* https://django-extensions.readthedocs.io/en/latest/
* https://developers.google.com/calendar/quickstart/python
* https://docs.djangoproject.com/en/3.0/ref/contrib/admin/admindocs/

