# ==== CONFIGURE
USERID ?= $$(id -u)
GROUPID ?= $$(id -g)
# import config.
# You can change the default config with `make cnf="config_special.env" build`
cnf ?= config.env
ifeq (,$(wildcard $(cnf)))
include $(cnf)
export $(shell sed 's/=.*//' $(cnf))
endif

# import deploy config
# You can change the default deploy config with `make dpl="deploy_special.env" release`
ifndef APP_NAME
	dpl ?= deploy.env
	include $(dpl)
	export $(shell sed 's/=.*//' $(dpl))
endif

# ====  HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## This help. Typical usage `make build run` and then `make publish` so that other systems can test it, then `make release`
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

define initializervalues =
@echo "Example:"
@echo
@echo "    APP_NAME=mysite DOCKER_REPO=registry.gitlab.com/myaccount make initialize"
@echo
@echo "Exiting."; exit 1;
endef

# ====  DOCKER TASKS


initialize: ## Create the initial files and project space - WARNING! only do this when starting a new project!
	
ifeq ($(APP_NAME),)
	@echo "Please specify a value for APP_NAME !!!"
	$(initializervalues)
endif
	
	
ifeq ($(DOCKER_REPO),)
	@echo "Please specify a value for DOCKER_REPO !!!"
	$(initializervalues)
endif

	@while [ -z "$$CONTINUE" ]; do \
		read -r -p "WARNING! This will remove all existing project files, are you sure? [y/N]: " CONTINUE; \
	done ; \
	[ $$CONTINUE = "y" ] || [ $$CONTINUE = "Y" ] || (echo "Exiting."; exit 1;)

	@rm -rf .git app deploy.env config.env

	@echo "APP_NAME=$(APP_NAME)" > deploy.env
	@echo "DOCKER_REPO=$(DOCKER_REPO)" >> deploy.env
	@touch config.env

	docker run \
		--rm \
		-w /app \
		-v ${PWD}/app:/app \
		-it python:3 /bin/bash -c \
		"pip3 install djangocms-installer && djangocms -s -w -p . engine"

	@echo "Your application files are now created in ./app"
	@echo "The next thing to do is following basic steps to initialize your local project repository, like so:"
	@echo "	git init"
	@echo "	git add -A"
	@echo "	git config user.email 'exampleuser@example.com'"
	@echo "	git config user.name 'Example User'"
	@echo "	git commit -m 'Initial import'"
	@echo "	git tag -a 0.0.1 -m 'Initial build' "
	@echo "...Of course, change the user values to your actual personal information."
	@echo "If you are not ready to use git, VERSION will be set to for all make commands automaticly."
	@echo "You will want to commit yourfiles to git and set an remote origin in git as soon as possible."

# Git the version from the most recent tag or the commandline
ifneq "$(wildcard ./.git)" ""
VERSION ?= $(shell git describe --tags --always --dirty)
else
VERSION ?= 0.0.1-needsinit
endif

clean: ## Remove all bytecode and cache copies
	find . -name "*.py[cod]" -exec rm -f {} \;

purge: clean ## Remove all bytecode and cache copies
	rm -rf app/static
	rm -rf app/media
	rm -f app/project.db

build: clean ## Build the container image (uses cached layers if possible)
	@echo "Building $(APP_NAME):$(VERSION)"
	docker build --build-arg UID=$(USERID) --build-arg GID=$(GROUPID) -t $(APP_NAME):$(VERSION) .

build-nc: clean ## Build the container image without caching (good for removing kruft from old builds - slower)
	@echo "Building $(APP_NAME):$(VERSION) without image caching"
	docker build --build-arg UID=$(USERID) --build-arg GID=$(GROUPID) --no-cache -t $(APP_NAME):$(VERSION) .


# ====  DOCKER DEVELOPMENT TASKS
populatedb:
	@echo "Loading fixture data"
	docker exec \
		-it $(APP_NAME) \
		bash -c './manage.py populate_db && ./manage.py loaddata fakefixtures.json'

develop:  ## Run the container with local application files mounted
	@echo "Running $(APP_NAME):$(VERSION) interactively with host bound volume"
	docker run \
		--rm \
		--add-host db:192.168.2.16 \
		--name $(APP_NAME) \
		--env-file=./config.env \
		--user $(USERID):$(GROUPID) \
		-p 8000:8000 \
		-i \
		-t \
		-d \
		-v ${PWD}/app:/app \
		$(APP_NAME):$(VERSION)
	@echo "Server is now running in the background. Use 'make stopdev' to stop it."
	@echo "Use 'docker logs -f makerspace' to watch the debugging and logging output,"
	@echo "Use 'docker exec -it makerspace bash -c \"./manage.py createsuperuser\" to create a new admin user"

migrations: ## Build container image and then generate migrations (develop first)
	@echo "Creating migrations"
	docker exec \
		-it $(APP_NAME) \
		bash -c "./manage.py makemigrations && ./manage.py migrate"

superuser: ## Create a superuser in the development container (develop first)
	@echo "Creating superuser"
	docker exec \
		-it $(APP_NAME) \
		bash -c './manage.py createsuperuser'

debug: ## Begin a log tail on the develop container
	@echo "Beggining log tail."
	docker logs -f makerspace

VERBOSITY ?= 0
test: ## Run all the tests
	@echo "Beggining tests"
	
	docker exec \
		-it $(APP_NAME) \
		bash -c './manage.py test --parallel -v $(VERBOSITY)'

stopdev: ## Stop the development container if it is running
	docker stop makerspace


# ====  Testrun


run: ## Run container with values configured in `config.env`
	@echo "Running $(APP_NAME):$(VERSION) in background.  Use 'docker stop $(APP_NAME)' to stop and remove"
	docker run \
		--rm \
		--name="$(APP_NAME)" \
		--env-file=./config.env \
		-p 8000:8000 \
		-d \
		$(APP_NAME):$(VERSION)


# ====  DOCKER IMAGE TASKS


# Docker publish
publish: repo-login publish-version ## Publish the `{version}` tagged container to Docker Hub

publish-latest: repo-login tag-latest
	@echo 'publish latest to $(DOCKER_REPO)'
	docker push $(DOCKER_REPO)/$(APP_NAME):latest

publish-version: tag-version
	@echo 'publish $(VERSION) to $(DOCKER_REPO)'
	docker push $(DOCKER_REPO)/$(APP_NAME):$(VERSION)

release: publish publish-latest ## Publish the `{version}` and `latest` tagged containers to Docker Hub


# Docker tagging
tag: tag-latest tag-version ## Generate container tags for the `{version}` and `latest` tags

tag-latest: ## Generate container `latest` tag
	@echo 'create tag latest'
	docker tag $(APP_NAME) $(DOCKER_REPO)/$(APP_NAME):latest

tag-version: ## Generate container `{version}` tag (uses latest tag from `git describe`)
	@echo 'create tag $(VERSION)'
	docker tag $(APP_NAME) $(DOCKER_REPO)/$(APP_NAME):$(VERSION)


# login to Docker Hub
repo-login: ## Login to Docker Hub (asks for credentials if they are not already set)
	@eval $(docker login)
