from django.contrib import admin

from mspace.models.profiles import Profile

# Register your models here.
<<<<<<< HEAD
<<<<<<< HEAD
from .models import Profile
from .models import Tool
from .models import Waiver
from .models import Guest
from .models import MemberNote
from .models import Skill
from .models import MemberSkill
from .models import MemberCertification
from .models import Position
from .models import MemberPosition
from .models import MemberType
from .models import MemberRegistration
from .models import ContactInfo
from .models import Purpose
from .models import Location

from .models.profiles import ProfileAdmin
from .models.memberskills import MemberSkillAdmin

class ProfileInline(admin.StackedInline):
    '''Tabular Inline View for Profile'''

    model = ProfileAdmin


# Register the models to Admin for
class AdminProfile(admin.ModelAdmin):
    inlines = [
        ProfileInline,
    ]
    list_display = [
        'user', 
        'bday', 
        'display_name', 
        #'over18',
        ]
    #fields = ['']
admin.site.register(Profile, AdminProfile)


# Register the models to Admin for
class AdminTool(admin.ModelAdmin):
    list_display = [
        'name',
        'description',
        ]
    
    #fields = ['']
admin.site.register(Tool, AdminTool)


# Register the models to Admin for
class AdminWaiver(admin.ModelAdmin):
    list_display = [
        'profile',
        'name',
        ]
    
    #fields = ['']
admin.site.register(Waiver, AdminWaiver)


# Register the models to Admin for
class AdminGuest(admin.ModelAdmin):
    list_display = [
        'guest',
        'sponsor',
        'guest_type'
        ]
    
    #fields = ['']
admin.site.register(Guest, AdminGuest)


# Register the models to Admin for
class AdminMemberNote(admin.ModelAdmin):
    list_display = [
        'member',
        'visibility',
        #'note',
        ]
    
    #fields = ['']
admin.site.register(MemberNote, AdminMemberNote)


# Register the models to Admin for
class AdminSkill(admin.ModelAdmin):
    list_display = [
        'name',
        'description',
        ]
    
    #fields = ['']
admin.site.register(Skill, AdminSkill)

class MemberSkillInline(admin.TabularInline):
    model=MemberSkillAdmin

# Register the models to Admin for
class AdminMemberSkill(admin.ModelAdmin):
    inlines = [
        MemberSkillInline,
    ]
#### TODO: Display admin file attributes in the list table page en/admin/mspace/memberskill/
    list_display = [
        'skill',
        'member',
        'level',
        ]
    
    #fields = ['']
admin.site.register(MemberSkill, AdminMemberSkill)


# Register the models to Admin for
class AdminMemberCertification(admin.ModelAdmin):
    list_display = [
        'member',
        'tool',
        'certifier',
        'date',
        'level',
        'note',
        ]
    
    #fields = ['']
admin.site.register(MemberCertification, AdminMemberCertification)


class AdminPosition(admin.ModelAdmin):
    list_display = [
        'name',
        'description',
        'responsibilities',
        ]
    
    #fields = ['']
admin.site.register(Position, AdminPosition)

class AdminMemberPosition(admin.ModelAdmin):
    list_display = [
        'member',
        'position',
        'approved',
        'paid',
    ]

    #fields = ['','','',]
admin.site.register(MemberPosition, AdminMemberPosition)

class AdminMemberType(admin.ModelAdmin):
    pass
    #list_display = [
    #    '',
    #    '',
    #]

    #fields = ['','','',]
admin.site.register(MemberType, AdminMemberType)

class AdminMemberRegistration(admin.ModelAdmin):
    pass
    #list_display = [
    #    '',
    #    '',
    #]

    #fields = ['','','',]
admin.site.register(MemberRegistration, AdminMemberRegistration)

class AdminContactInfo(admin.ModelAdmin):
    pass
    #list_display = [
    #    '',
    #    '',
    #]

    #fields = ['','','',]
admin.site.register(ContactInfo, AdminContactInfo)

# Register the models to Admin for
class AdminPurpose(admin.ModelAdmin):
    list_display = [
        'name',
        'description',
        ]
    
    #fields = ['']
admin.site.register(Purpose, AdminPurpose)

# Register the models to Admin for
class AdminLocation(admin.ModelAdmin):
    list_display = [
        'name',
        'description',
        ]
    
    #fields = ['']
admin.site.register(Location, AdminLocation)
=======
class Admin(admin.ModelAdmin):
    list_display = ['user','employer','first_name']

admin.site.register(Profile, Admin)
>>>>>>> Add the Profile model to the admin page
=======
class Admin(admin.ModelAdmin):
    list_display = ['user','employer','first_name']

admin.site.register(Profile, Admin)
>>>>>>> 6c52d13429a05699bdbe3cc2ce5d20d93de33c65
