from django.test import TestCase
from mspace.models import Tool

#Thank you Mozilla for provideing the examples. https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Testing
class basicTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        
        cls.basic = Tool.objects.create(name='This', description="That")
        
        # This is equivalent to the line above
        Tool.objects.create(name='NoDescription')
        cls.noDescript = Tool.objects.get(id=2)

    def test_name_label(self):
        # Get the metadata for the required field and use it to query the required field data
        field_label = self.basic._meta.get_field('name').verbose_name
        
        # Compare the value to the expected result
        self.assertEquals(field_label, 'name')
    
    def test_description_label(self):
        field_label = self.basic._meta.get_field('description').verbose_name
        self.assertEquals(field_label, 'description')    

    def test_name_max_length(self):
        max_length = self.basic._meta.get_field('name').max_length
        self.assertEquals(max_length, 100)

    def test_object_name(self):
        self.assertEqual(str(self.basic), self.basic.name)

    def test_description_output(self):
        description = self.basic.description
        self.assertEquals(description, "That")
    
    def test_blank_description(self):
        description = self.noDescript.description
        self.assertEquals(description, None)
