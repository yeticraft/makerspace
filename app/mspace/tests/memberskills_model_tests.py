from django.test import TestCase
from django.contrib.auth.models import AnonymousUser, User
from mspace.models import Skill
from mspace.models import Profile
from mspace.models import MemberSkill
from mspace.models.memberskills import MemberSkillAdmin
from datetime import date


class MemberSkillTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(username='JohnDoe', email='JBlow@blrabla', password='not_secret')
        member = Profile.objects.get(id=1)
        skill = Skill.objects.create(name="Test Skill", description="A descripition")
        MemberSkill.objects.create(member=member, skill=skill, level="int")
        MemberSkillAdmin.objects.create(skill=MemberSkill.objects.get(id=1), endorsement_level='ski', endorser=member, endorsementDate=date.today())

        cls.memberskill = MemberSkill.objects.get(id=1)
        cls.memberskilladmin = MemberSkillAdmin.objects.get(id=1)

    
    def test_level_max_length(self):
        max_length = self.memberskill._meta.get_field('level').max_length
        self.assertEquals(max_length,3)

    def test_object_name(self):
        self.assertEquals(str(self.memberskill), self.memberskill.skill.name)
    
    def test_member_label(self):
        field_label = self.memberskill._meta.get_field('member').verbose_name
        self.assertEquals(field_label, 'Member')
    
    def test_skill_label(self):
        field_label = self.memberskill._meta.get_field('skill').verbose_name
        self.assertEquals(field_label, 'Skill')

    def test_level_label(self):
        field_label = self.memberskill._meta.get_field('level').verbose_name
        self.assertEquals(field_label, 'level')

    def test_level_name(self):
        skill_level = self.memberskill.level
        self.assertEquals(str(skill_level), "int")

    def test_endorser_label(self):
        field_label = self.memberskilladmin._meta.get_field('endorser').verbose_name
        self.assertEquals(field_label, 'Endorser')
        
    def test_endorsementlevel_length(self):
        length = self.memberskilladmin._meta.get_field('endorsement_level').max_length
        self.assertEqual(length, 3)
    
    def test_endorsementlevel_name(self):
        field_label = self.memberskilladmin._meta.get_field('endorsement_level').verbose_name
        self.assertEqual(field_label, 'endorsement level')