from django.test import TestCase
from mspace.models import Skill 

#Thank you Mozilla for provideing the examples. https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Testing
class SkillTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.skill = Skill.objects.create(name="SKILL_A")

    def test_object_name(self):
        self.assertEqual(str(self.skill), self.skill.name)

