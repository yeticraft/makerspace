from django.test import TestCase
from mspace.models import Tool

#Thank you Mozilla for provideing the examples. https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Testing
class ToolTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up data for the whole TestCase
        
        # Reference cls.tool as self.tool in tests
        cls.tool = Tool.objects.create(name='Drill', description="spinn thing")
        
        # This is equivalent to the line above
        Tool.objects.create(name='NoDescription')
        cls.noDescript = Tool.objects.get(id=2)

    def test_object_name(self):
        self.assertEqual(str(self.tool), self.tool.name)


