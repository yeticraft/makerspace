from django.contrib.auth.models import AnonymousUser, User
from django.test import TestCase
from mspace.models import Profile

class ProfileTest(TestCase):
    @classmethod
    def setUpTestData(cls):        
        User.objects.create_user(username='JoeBlow', email='JBlow@blabla', password='not_secret')
        cls.thisuser = Profile.objects.get(id=1)

    def test_display_name_label(self):
        filed_label = self.thisuser._meta.get_field('display_name').verbose_name
        self.assertEquals(filed_label,'display name')
    
    def test_display_name_max_length(self):
        max_length = self.thisuser._meta.get_field('display_name').max_length
        self.assertEquals(max_length,100)

    def test_object_name(self):
        self.assertEquals(str(self.thisuser), self.thisuser.user.username)