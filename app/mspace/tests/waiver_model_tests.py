from django.test import TestCase
from django.contrib.auth.models import User
from mspace.models import Waiver
from mspace.models import Profile

#Thank you Mozilla for provideing the examples. https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Testing
class WaiverTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(username='JohnDoe', email='JBlow@blrabla', password='not_secret')
        member = Profile.objects.get(id=1)
        cls.waiver = Waiver.objects.create(profile=member)
    
    def test_name_label(self):
        field_label = self.waiver._meta.get_field('name').verbose_name
        self.assertEquals(field_label, 'Waiver type')
    
    def test_name_max_length(self):
        max_length = self.waiver._meta.get_field('name').max_length
        self.assertEquals(max_length, 30)
    
    def test_name_default(self):
        default_name = self.waiver.name
        self.assertEqual(default_name, 'hold harmless')

    def test_object_name(self):
        self.assertEqual(str(self.waiver), self.waiver.name)

    def test_image_label(self):
        field_label = self.waiver._meta.get_field('image').verbose_name
        self.assertEquals(field_label, 'Scan')
    
    def test_image_blank(self):
        image = self.waiver.image
        self.assertEqual(image, None)
