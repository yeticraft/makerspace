from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from mspace.models import Skill, Position, Location, Tool, Purpose, MemberType, MemberSkill, Profile, MemberPosition, MemberCertification, MemberNote
from django_seed import Seed
import json

tools = [
    "Saw",
    "Hammer",
    "Cultivator",
    "Rake",
    "Ladder",
    "File",
    "Gloves",
    "Wheelbarrow",
    "mallet",
    "chisel",
    "screwdriver",
    "wrench",
    "hand drill",
    "level",
    "ax",
    "pliers",
    "clamp",
    "bolt cutter",
    "box cutter",
    "shears",
    "vise",
    "nut",
    "bolt",
    "nail",
    "screw",
    "tape measure",
    "shovel",
    "Allen key",
    "chainsaw",
    "heat gun",
    "jackhammer",
    "circular saw",
    "nail gun",
    "drill",
    "sander",
    "grinder",
    "diamond tool",
    "lathe",
    "lawn mower",
    "pneumatic torque wrench",
    "table saw",
    "router",
    "3D Printer",
    "Laser Cutter",
]

skill_list = [
    "Airbrushing",
    "Beading",
    "Blacksmithing",
    "Bridge ",
    "building",
    "Calligraphy",
    "Candle making",
    "Cartoons",
    "Carving",
    "Crochet",
    "Cross Stich",
    "Dollhouses",
    "Drawing",
    "Embroidery",
    "Engraving",
    "Fly tying",
    "fly fishing",
    "Glass blowing",
    "Graphic Design",
    "Jewelry making",
    "Knitting",
    "Leather crafting",
    "Macramé",
    "Map making",
    "Model Cars",
    "Model rockets",
    "Model Ships",
    "Needlepoint",
    "Origami",
    "Painting",
    "Photography",
    "Pottery",
    "Quilting",
    "Scrapbooking",
    "Sculpture",
    "Sewing",
    "Soap making",
    "Stained Glass",
    "Taxidermy",
    "Tie dying",
    "Weaving",
    "Woodworking",
]

class Command(BaseCommand):
    
    def _create_db(self):
        args = '<foo bar ...>' 
        help = 'Fill the database with fake data'
        print("Starting Seeder")
        seeder = Seed.seeder()

        print("---> Adding Users to the seeder.")
        seeder.add_entity(User, 50)

        print("---> Adding Skills to the seeder.")
        seeder.add_entity(Skill, len(skill_list),{
            'name': lambda x: seeder.faker.random_element(elements=skill_list)
        })
        
        print("---> Adding Positions to the seeder.")
        seeder.add_entity(Position, 10,{
            'name' : lambda x: seeder.faker.job()
        })

        seeder.add_entity(Location,20,{
            'name' : lambda x: seeder.faker.word()
        })

        seeder.add_entity(Tool, len(tools),{
            'name' : lambda x: seeder.faker.random_element(elements=tools)
        })

        seeder.add_entity(Purpose, 20,{
            'name' : lambda x: seeder.faker.word()
        })
        
        seeder.add_entity(MemberSkill, 50, {
            'member' : lambda x: Profile.objects.get(id = seeder.faker.pyint(min_value=1, max_value=49))
        })

        seeder.add_entity(MemberType, 10,{
           'name' : lambda x: seeder.faker.word()
        })

        seeder.add_entity(MemberCertification,50,{
            'member' : lambda x: Profile.objects.get(id = seeder.faker.pyint(min_value=1, max_value=49)),
            'certifier'  : lambda x: Profile.objects.get(id = seeder.faker.pyint(min_value=1, max_value=49)),
            'level' : lambda x: seeder.faker.random_element(elements=('not', 'use', 'ass', 'cer',))
        })

        print("---> Adding then following Seeder entities to the database:")
        inserted_pks = seeder.execute()
        for x in inserted_pks:
            modelname = str(x)
            print("%s : %s" %(modelname, len(inserted_pks[x])))

        print("Adding Tracked Models.")
        models = ["membernote", "memberposition", "memberregistration" ]
        # This next section takes car of the TrackedModels
        data = []
        for model in models:
            print("---> Adding %s." %(model))
            
            for i in range(len(data), len(data)+20,):

                # Create the TrackedModel specific data        
                data.append({
                    'model' : "mspace." + model,
                    'pk' : i,
                    'fields' :{
                        'created' : seeder.faker.iso8601()+"Z",
                        'created_by' : seeder.faker.pyint(min_value=1, max_value=49),
                        'modified' : seeder.faker.iso8601()+"Z",
                        'modified_by' : seeder.faker.pyint(min_value=1, max_value=49),
                        'remote_addr' : seeder.faker.ipv4(network=False, address_class=None, private=None),
                        'member' : seeder.faker.pyint(min_value=1, max_value=49),
                    }
                })

                # Add in the model specific data
                this = data[i]
                if model =="membernote":
                    this['fields'].update({
                        'note' : seeder.faker.sentence(),
                        'visibility' : seeder.faker.random_element(elements=("admin", "admem", "publi", "membr"))
                        }
                    )
                elif model =="memberposition":
                    this['fields'].update({
                        'position' : seeder.faker.pyint(min_value=1, max_value=10),
                        'approved' : seeder.faker.boolean(),
                        'paid' : seeder.faker.boolean()
                        }
                    )
                elif model =="memberregistration":
                    this['fields'].update({
                        'type' : seeder.faker.pyint(min_value=1, max_value=10),
                        'isActive' : seeder.faker.boolean(),
                        'expires' : seeder.faker.date()
                    })

        print("Creating fixtures file.")
        with open('mspace/fixtures/fakefixtures.json', 'w') as outfile:
            json.dump(data, outfile)

    def handle(self, *args, **options):
        self._create_db()