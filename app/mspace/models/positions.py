from .abstract import BasicModel
from django.db import models


class Position(BasicModel):
    """Model definition for Position.
    
    Attributes
    ----------
    name : str
        name of Position
    description : str
        describes the postion
    responsibilities: str
        what duties are required of this a person in this postion.

    Methods
    -------
    None

    Returns
    -------
    The position Name
    """

    responsibilities = models.TextField(blank=True, null=True)


    class Meta:
        """Meta definition for Position."""

        verbose_name = 'Position'
        verbose_name_plural = 'Positions'


    def __str__(self):
        """Unicode representation of Position."""
        return self.name
