from django.db import models
from django.utils.translation import ugettext as _, ugettext_noop as _noop
from .profiles import Profile
from .tools import Tool


LEVEL_TYPES = (
    ("not","not certified"),
    ("use","can use"),
    ("ass","can assist"),
    ("cer","can certify"),
)

class MemberCertification(models.Model):
    """Model definition for MemberCertification.
    
    Attributes
    ----------
        member : ForeignKey 
            Who earned the certification
        tool : ForeignKey
            What tool they earned the certification on
        certifier : ForeignKey
            Who granted them the certification
        Date : DateField
            The date and time 
        Level : CharField
            0=not, 1=can use, 2=can assist, 3=can certify - expert and admin controlled
        Notes : TextField
            Notes on the member certifications
    
    Returns
    -------
        The member Name
    """

    member = models.ForeignKey('Profile', verbose_name=_("Member"), related_name='member_certification_member', on_delete=models.CASCADE)
    tool = models.ForeignKey('Tool', verbose_name=_("Tool"), related_name='tool_ID', on_delete=models.CASCADE)
    note = models.TextField(blank=True, null=True)
    #### TODO: Note the following should be restricted to admins and members who are marked as "can certify"
    level = models.CharField(max_length=3, choices=LEVEL_TYPES, default="not")
    certifier = models.ForeignKey('Profile', verbose_name=_("Certifier"), related_name='certifier_ID', on_delete=models.CASCADE)
    date = models.DateField()
    

    class Meta:
        """Meta definition for MemberCertification."""

        verbose_name = 'Member Certification'
        verbose_name_plural = 'Member Certifications'

    def __str__(self):
        """Unicode representation of MemberCertification."""
        return self.member.user.username
