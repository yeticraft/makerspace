from django.db import models
from django.utils.translation import ugettext as _, ugettext_noop as _noop
from .profiles import Profile
from .purposes import Purpose
from .locations import Location
from .abstract import TrackedModel

class Login(TrackedModel):
    """Model definition for Login.A tracking of physical logins fed from card readers and door locks.
    
    Attributes
    ----------
        member = ForeignKey
            Who logged in
        purpose = ForeignKey
            Why they are here
        logindatetime = DateTime
            Date and time of loggin
        locationID = ForeignKey
            What door they entered

    Returns
    -------
        The member name
    """

    member = models.ForeignKey(Profile, verbose_name=_("Member"), related_name='login_member', on_delete=models.CASCADE)
    purpose = models.ForeignKey(Purpose, verbose_name=_("Perpose"), related_name='login_purpose', on_delete=models.CASCADE)
    logindatetime = models.DateTimeField(auto_now=True)
    locationID = models.ForeignKey(Location, verbose_name=_("Location"), related_name='login_location', on_delete=models.CASCADE)

    class Meta:
        """Meta definition for Login."""

        verbose_name = 'Login'
        verbose_name_plural = 'Logins'

    def __str__(self):
        """Unicode representation of Login."""
        return self.member.user.username
