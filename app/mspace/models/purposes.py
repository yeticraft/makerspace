from .abstract import BasicModel

class Purpose(BasicModel):
    """Model definition for Purpose."""

    class Meta:
        """Meta definition for Purpose."""

        verbose_name = 'Purpose'
        verbose_name_plural = 'Purposes'

    def __str__(self):
        """Unicode representation of Purpose."""
        return self.name
