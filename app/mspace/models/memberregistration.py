from django.db import models
from django.utils.translation import ugettext as _, ugettext_noop as _noop
from .profiles import Profile
from .membertypes import MemberType
from .abstract import TrackedModel


class MemberRegistration(TrackedModel):
    """Model definition for MemberRegistration.
    
    Attributes
    ----------
        member : Profile
            Who has registerd
        isActive : boolean 
            calculated from Payments, guest startdate, or affiliated record with resident member
        type : ForeignKey 
            What type of registration they have. (guest, residentguest, student, donor, regular, resident, board, etc)
        expires : date 
            When will they have to renew. Boardmember is set infinite, resident guests are same a sponsor)

    Returns
    -------
        The registered members name
    
    """

    member = models.ForeignKey(Profile, verbose_name=_("Member"), related_name='member_Id', on_delete=models.CASCADE)
    isActive = models.BooleanField (verbose_name=("Is Active"), default=False)
    type = models.ForeignKey(MemberType,verbose_name=_("Type"), related_name='type_ID', on_delete=models.CASCADE)
    expires = models.DateField()
    
    class Meta:
        """Meta definition for MemberRegistration."""

        verbose_name = 'Member Registration'
        verbose_name_plural = 'Member Registrations'

    def __str__(self):
        """Unicode representation of MemberRegistration."""
        return self.member.user.username
