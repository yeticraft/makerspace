from django.db import models
from django.utils.translation import ugettext as _, ugettext_noop as _noop
from .profiles import Profile
from .abstract import TrackedModel

class ContactInfo(models.Model):
    """Model definition for ContactInfo.
    
    TODO: Add additional attributes
    """

    member = models.ForeignKey(Profile, verbose_name=_("Member"), related_name='ContactInfo_member', on_delete=models.CASCADE)
    # Data (needs definition - supports emergency, social media, phone, email, etc)


    class Meta:
        """Meta definition for ContactInfo."""

        verbose_name = 'Contact Info'
        verbose_name_plural = 'Contact Infos'

    def __str__(self):
        """Unicode representation of ContactInfo."""
        return self.member.user.username 
