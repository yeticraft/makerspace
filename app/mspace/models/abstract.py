from django.db import models
from django.utils.translation import ugettext as _, ugettext_noop as _noop
from crum import get_current_request
from crum import get_current_user


class TrackedModel(models.Model):
    """ Adds Abstract class that provides the following basic access tracking 

    Attributes
    ----------
        created : DateTime
            
        created_by : ForeignKey
            auth.User
        modified : DateTime

        modified_by : ForeignKey
            auth.User
        remote_addr : str
            IP address
    """

    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey('auth.User', related_name='%(app_label)s_%(class)s_creator', blank=True, null=True, default=None, on_delete = models.CASCADE)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey('auth.User', related_name='%(app_label)s_%(class)s_modifier', blank=True, null=True, default=None, on_delete = models.CASCADE)
    remote_addr = models.CharField(max_length=15, blank=True, default='')

    def save(self, *args, **kwargs):
        request = get_current_request()
        if request and not self.remote_addr:
            self.remote_addr = request.META['REMOTE_ADDR']
        super().save(*args, **kwargs)

        user = get_current_user()
        if user and not user.pk:
            user = None
        if not self.pk:
            self.created_by = user
        self.modified_by = user
        super().save(*args, **kwargs)

    class Meta:
    # Set model as "abstract base class"
        abstract = True

class BasicModel(models.Model):
    name = models.CharField(max_length=100) #Should probably enforce unique=True
    description = models.TextField(blank=True, null=True)

    class Meta:
        abstract = True
