from django.db import models
from django.utils.translation import ugettext as _, ugettext_noop as _noop
from .profiles import Profile


def user_waiver_path(instance, filename): 
    # file will be uploaded to MEDIA_ROOT / user_<id>/<filename> 
    return 'profiles/user_{0}/{1}'.format(instance.profile.user.id, 'picture') 


class Waiver(models.Model):
    """Model definition for waiver."""

    name = models.CharField('Waiver type', max_length=30, default='hold harmless', editable=False)
    profile = models.ForeignKey(Profile, verbose_name=_(""), on_delete=models.CASCADE)
    image = models.ImageField('Scan', upload_to = user_waiver_path, blank=True, null=True)

    class Meta:
        """Meta definition for waiver."""

        verbose_name = 'waiver'
        verbose_name_plural = 'waivers'

    def __str__(self):
        """Unicode representation of waiver."""
        return f"{self.name}"
