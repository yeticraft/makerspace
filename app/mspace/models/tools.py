from .abstract import BasicModel

class Tool(BasicModel):
    """Model definition for TOOLs."""

    class Meta:
        """Meta definition for Tool."""
    
        verbose_name = 'tool'
        verbose_name_plural = 'tools'

    def __str__(self):
        """Unicode representation of TOOL."""
        return self.name
