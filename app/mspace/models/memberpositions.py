from django.db import models
from django.utils.translation import ugettext as _, ugettext_noop as _noop
from .profiles import Profile
from .positions import Position
from .abstract import TrackedModel

class MemberPosition(TrackedModel):
    """Model definition for MemberPosition.
    
    Attributes
    ----------
        member : Model
            Who earned the position
        position : Model
            What position they are now
        approved : bool
            Has this members position been approved
        paid : bool
            Is this member on the payroll

    Returns
    ------
        The members name
    """
    position = models.ForeignKey(Position, verbose_name=_("Position"), related_name='positionsID', on_delete=models.CASCADE)
    member = models.ForeignKey(Profile, verbose_name=_("Member"), related_name='membersID', on_delete=models.CASCADE)
    approved = models.BooleanField(default=False)
    paid = models.BooleanField(default=False)

    class Meta:
        """Meta definition for MemberPosition."""

        verbose_name = 'Member Position'
        verbose_name_plural = 'Member Positions'

    def __str__(self):
        """Unicode representation of MemberPosition."""
        return self.member.user.username
