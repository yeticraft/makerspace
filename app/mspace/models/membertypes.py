from .abstract import BasicModel


class MemberType(BasicModel):
    """Model definition for MemberType.
    
    Attributes
    ----------
    name : str
        Name of member type
    description : str
        Member type description
    """

    class Meta:
        """Meta definition for MemberType."""

        verbose_name = 'Member Type'
        verbose_name_plural = 'Member Types'

    def __str__(self):
        """Unicode representation of MemberType."""
        return self.name
