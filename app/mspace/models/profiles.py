from django.db import models
from django.db.models.signals import post_save 
from django.dispatch import receiver
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _, ugettext_noop as _noop
from datetime import date
User = get_user_model()


def user_picture_path(instance, filename): 
    # file will be uploaded to MEDIA_ROOT / user_<id>/<filename> 
    return 'profiles/user_{0}/{1}'.format(instance.user.id, 'picture') 

def user_avatar_path(instance, filename): 
    # file will be uploaded to MEDIA_ROOT / user_<id>/<filename> 
    return 'profiles/user_{0}/{1}'.format(instance.user.id, 'avatar') 

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to = user_avatar_path, blank=True, null=True)
    bday = models.DateField(blank=True, null=True)
    display_name = models.CharField(max_length=100, blank=True, null=True)
    bio = models.TextField(blank=True, null=True)

    # isActive (calculated from Logins < 30 days)
    
    def __str__(self):
        return self.user.username

    #### TODO: Verify the following functions and properties
    @property
    def over18(self):
        self.setover18()
        return self._over18

    @over18.setter  
    def over18(self, value):
        self._over18 = False
        if self.bday != None:
            age = date.today() - self.bday
            if age.days >= (18*365) :
                self._over18 = True
    
    def setover18(self):
        self.over18 = False
    
    def get_displayname(self):
        dis_name=self.user.first_name
        if self.display_name:
            dis_name=self.display_name 
        return dis_name

# these are the signals
@receiver(post_save, sender=User) 
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User) 
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


# Administrative Profile data
class ProfileAdmin(models.Model):
    profile = models.OneToOneField(Profile, on_delete=models.CASCADE)
    haswaiver = models.BooleanField(default=False)
    picture = models.ImageField(upload_to = user_picture_path, blank=True, null=True)

    def __str__(self):
        return self.profile.user.username

    #### NOTE: This may need to be in the ProfileAdmin class due to calling the self.picture that resides their.
    def get_avatar(self):
        avatar = self.profile.avatar
        if avatar == None:
            avatar = self.picture
        return avatar
