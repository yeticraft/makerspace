from django.db import models
from django.utils.translation import ugettext as _, ugettext_noop as _noop


class Calendar(TrackedModel):
    """Model definition for Calendar.
    This is designed to be compatable with https://developers.google.com/calendar/v3/reference/Calendars
    
    Attributes
    ----------
    timeZone : string
     	The time zone of the calendar. 
        (Formatted as an IANA Time Zone Database name, e.g. "Europe/Zurich".)
        Optional.
    tool : id
        The calendar used to schedule tool usage.
        Optional.
    
    Returns
    ------
        The Calendar summary
    """

    summary = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    location = models.TextField(blank=True, null=True)
    timezone = models.TextField(blank=True, null=True)
    tool = models.ForeignKey('Tool', verbose_name=_("Tool"), related_name='calendar_tool_ID', on_delete=models.CASCADE, blank=True, null=Tru)
    
    class Meta:
        """Meta definition for Calendar."""

        verbose_name = 'Calendar'
        verbose_name_plural = 'Calendars'

    def __str__(self):
        """Unicode representation of Calendar."""
        return self.summary
