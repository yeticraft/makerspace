from django.db import models
from django.utils.translation import ugettext as _, ugettext_noop as _noop
from .profiles import Profile
from .abstract import TrackedModel
from .calendars import Calendar


VISIBILITY_TYPES = (
    ("def","default"),
    ("pub","public"),
    ("pri","private"),
    ("con","confidential"),
)

class Event(TrackedModel):
    """Model definition for Event.
    This is designed to be compatable with https://developers.google.com/calendar/v3/reference/events
    
    Attributes
    ----------

    summary : string
        Title of the event.
    end : nested object
        end.date : date
        end.dateTime : datetime
        end.timeZone : string
        Required.
    start : nested object
        start.date : date
        start.dateTime : datetime
        start.timeZone : string
        Required.
    calendar : id
        The calendar that this event is attached to.
        Optional.
    attendees : id
        List of member profiles.
    
    Returns
    ------
        The event summary
    """

    summary = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    location = models.TextField(blank=True, null=True)
    start =  models.DateTimeField()
    end =  models.DateTimeField()
    calendar = models.ForeignKey(Calendar, verbose_name=_("Calendar"), related_name='EventCalendarID', on_delete=models.CASCADE)
    attendees = models.ForeignKey(Profile, verbose_name=_("Attendees"), related_name='EventAttendeesID', on_delete=models.CASCADE)
    additionalGuests = models.IntegerField(default=0) # TODO: add `validators=[MinValueValidator(0, message="Value to low. Minimum is Zero"), MaxLengthValidator(20, message="Value to high. Maximum is 20")]
    visibility = models.CharField(max_length=3, choices=VISIBILITY_TYPES)
    organizer = models.ForeignKey(Profile, verbose_name=_("Organizer"), related_name='EventOrganizerID', on_delete=models.CASCADE)

    class Meta:
        """Meta definition for Event."""

        verbose_name = 'Event'
        verbose_name_plural = 'Events'

    def __str__(self):
        """Unicode representation of Event."""
        return self.summary
