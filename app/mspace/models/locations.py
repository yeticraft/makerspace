from .abstract import BasicModel

class Location(BasicModel):
    """Model definition for Location.
        which swipe location - example: front door, back, rooftop, teleporter, etc

    Attributes
    ----------
        name : str
            Location: door, moble_unit, etc
        description:

    Returns
    -------
        The locations name.
    """

    class Meta:
        """Meta definition for Location."""

        verbose_name = 'Location'
        verbose_name_plural = 'Locations'

    def __str__(self):
        """Unicode representation of Location."""
        return self.name
