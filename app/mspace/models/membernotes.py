from django.db import models
from django.utils.translation import ugettext as _, ugettext_noop as _noop
from .profiles import Profile
from .abstract import TrackedModel


VISIBILITY_TYPES = (
    ("admin","Admins Only"),
    ("admem","Admins and Members"),
    ("membr","Members"),
    ("publi","Public")
)


class MemberNote(TrackedModel):
    """Model definition for MemberNote.
    
    Attributes
    ----------
        member : ForeignKey
            Who is this note about
        visibility : char
            Who can see the note
        note : str
            The note itself
    Returns
    -------
        The name of who the notes about.
    """

    member = models.ForeignKey(Profile, verbose_name=_("Member"), related_name='member_id', on_delete=models.CASCADE)
    visibility =  models.CharField(max_length=5, choices=VISIBILITY_TYPES, default='admin')
    note = models.TextField()

    class Meta:
        """Meta definition for MemberNote."""

        verbose_name = 'Member Note'
        verbose_name_plural = 'Member Notes'

    def __str__(self):
        """Unicode representation of MemberNote."""
        return self.member.user.username
