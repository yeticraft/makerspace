from django.db import models
from .abstract import BasicModel

class Skill(BasicModel):
    """Model definition for Skill."""

    class Meta:
        """Meta definition for Skill."""

        verbose_name = 'Skill'
        verbose_name_plural = 'Skills'

    def __str__(self):
        """Unicode representation of Skill."""
        return self.name
