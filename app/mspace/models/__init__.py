# Main data groups (###) delineate access as well as purpose
# Tables (non-indented #) define table construction
# Fields (indented #) define fields, type, access, and intention
# ID fields are all assumed to exist, as is row level access-journaling/logging

### MEMBERS
# Profiles
    # hasWaiver (boolean - calculated from Waivers)
    # isActive (calculated from Logins < 30 days)
    # Picture (taken by registration process - admin controlled)
    # Icon (avatar image 64x64)
    # BIO (text - optional)
    # Displayname (can differ from login)
    # Firstname (legal)
    # Lastname (legal)
    # Birthday (month/year - optional)
    # Over18 (boolean)
    # Employer (text - optional)
from .profiles import Profile

# Waivers (a list of waivers that the member has on file)
    # MemberID (Profiles)
    # Image (imagefile - scan of signed waiver)
from .waivers import Waiver

# Guests (list of 'known' guests for this member - mostly for resident members)
    # GuestID (MemberID - Profiles)
    # SponsorID (MemberID - Profiles or Registrations resident member)
    # Type select(makerspace, resident, member)
from .guests import Guest

# MemberNotes (list of notes about the member, made by other members as well)
    # MemberID (Profiles)
    # CreatorID (MemberID - Profiles)
    # Visibility select(admins_only, admin_and_member, members, public)
    # NoteContent
from .membernotes import MemberNote

# MemberSkills (list of skills for the member)
    # SkillID (Skills)
    # MemberID (Profiles)
    # Level select(interested, skilled, teacher, jedi, etc)
    # Endorsementlevel select(admin controlled)
    # EndorserID (MemberID - Profiles)
    # EndorsementDate (Date)
from .memberskills import MemberSkill

# Skills (list of skill definitions)
    # Name
    # Description
from .skills import Skill

# Contactinfo (TBD - will use existing schema template)
    # MemberID (Profiles)
    #### TODO:
    # Data (needs definition - supports emergency, social media, phone, email, etc)
from .contactinfo import ContactInfo

# Logins (physical - fed from card readers and door locks)
    # MemberID (Profiles)
    # PurposeID (Purposes)
    # DateTimeStamp
    # LocationID (Locations)
from .logins import Login

# Purposes (personal-project, volunteer, teaching, shophost, etc)
    # name 
    # description
from .purposes import Purpose

# Locations (which swipe location - example: front door, back, rooftop, teleporter, etc)
    # name
    # description
from .locations import Location


### CERTIFICATIONS
# MemberCertifications
    # MemberID (Profiles)
    # ToolID (Tools)
    # CertifierID (MemberID - Profiles)
    # CertificationDate (DateTime)
    # CertificationLevel (0=not, 0.5=requires assist 1=can use, 2=can assist, 3=can certify - expert and admin controlled)
    # Notes
from .membercertifications import MemberCertification

### REGISTRATION
# MemberRegistration 
    # MemberID (Profiles)
    # Date (DateTimeStamp)
    # isActive (boolean - calculated from Payments, guest startdate, or affiliated record with resident member)
    # Type select (guest, residentguest, student, donor, regular, resident, board, etc)
    # Expires (date - boardmember is set infinite, resident guests are same a sponsor)
from .memberregistration import MemberRegistration

# MemberTypes
    # Name
    # Description
from .membertypes import MemberType

# Payments (includes volunteer hours value and also permenent)
    # TBD - use existing payment gateway schema with extra fields for volunteering, etc


### STAFF
# MemberPostitions
    # MemberID
    # PositionID (Positions)
    # Approved (boolean)
    # Paid (boolean)
from .memberpositions import MemberPosition

# Positions
    # Name
    # Description
    # Responsibilities
from .positions import Position


### FACILITIES
# Tools
    # ToolName
    # Description
    #### TODO:
    # Charge - units/min. to cust.
    # Cost - estimated elec. + materials + maintainance
    # AddressID (Address)
from .tools import Tool


### SCHEDULING (TrackedModels)
# ToolInUse
    # ToolID (Tools)    
    # MemberID (Profiles)
    # Date (Date)
    # TimeBlocks (Time) XXmin. intervals for each tool
    # Visibility select(admins_only, members, public)
    # Method - CanUse (boolean) True if Member CertificationLevel > 0 & is Active

# MemberAvailabilities
    # MemberID
    # Days
    # TimesPerday

# Shophosts
    # TBD

# Hostassignments
    # TBD

# Shophours
    # TBD

# Events (may be offsite - like LinuxFest or Saberfest)
    # Name
    # Date
    # TimeBlock
    # Description
    # Link - website
    # Repetition - OneOff, Daily, Weekly, Monthy, Yearly
    # AddressID (Address)
    # LocationID (EventLocation)

# Classes (onsite/online/Live educational event)
    # Name
    # Date
    # TimeBlock
    # Description
    # Link - website
    # Repetition - OneOff, Daily, Weekly, Monthy, Yearly
    # AddressID    
    # ToolID (Tools)
    # SkillID (Skills)

# ClassEvents
    # ClassID
    # EventID
    # Teacher (MemberID - Profile)


### REFERRALS (some way of tracking how folx got here.   one-direction offering of quotes for admins to cherry pick from)
    # ReferredByID (MemberID - Profiles) - Can be NONE
    # MemberID (Profiles)
    # advertisementType (website, flyer, word of mouth,, etc.)
    # quote (string)

