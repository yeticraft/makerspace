from django.db import models
from django.utils.translation import ugettext as _, ugettext_noop as _noop
from .profiles import Profile
from .skills import Skill


LEVEL_TYPES = (
    ("int","interested"),
    ("ski","skilled"),
    ("tea","teacher"),
    ("jed","jedi"),
)


class MemberSkill(models.Model):
    """Model definition for MemberSkill."""

    skill = models.ForeignKey(Skill, verbose_name=_("Skill"), related_name='skill_ID', on_delete=models.CASCADE)
    member = models.ForeignKey(Profile, verbose_name=_("Member"), related_name='member_ID', on_delete=models.CASCADE)
    level = models.CharField(max_length=3, choices=LEVEL_TYPES)


    class Meta:
        """Meta definition for MemberSkill."""

        verbose_name = 'Member Skill'
        verbose_name_plural = 'Member Skills'

    def __str__(self):
        """Unicode representation of MemberSkill."""
        return self.skill.name

class MemberSkillAdmin(models.Model):
    skill = models.OneToOneField(MemberSkill, on_delete=models.CASCADE)
    endorsement_level = models.CharField(max_length=3, choices=LEVEL_TYPES)
    endorser = models.ForeignKey(Profile, verbose_name=_("Endorser"), related_name='endorser_id', on_delete=models.CASCADE)
    endorsementDate = models.DateField()

    def __str__(self):
        return self.skill.skill.name