from django.db import models
from django.utils.translation import ugettext as _, ugettext_noop as _noop
from .profiles import Profile


GUEST_TYPES = (
    ("day","day pass"),
    ("res","resident guest"),
    ("adm","admin"),
)


class Guest(models.Model):
    """Model definition for GUESTs. 
        A list of 'known' guests for this member - mostly for resident members
    
    Attributes
    ----------
        guest : ForeignKey
            Who the guest is
        sponsor : ForeignKey
            The Member who invited them
        guest_type :


    Returns
    -------
        Name of the Guest
    """

    guest =  models.ForeignKey(Profile, verbose_name=_("Guest"), related_name='guest_guest', on_delete=models.CASCADE)
    sponsor = models.ForeignKey('Profile', verbose_name=_("Sponsor"), related_name='guest_sponsor', on_delete=models.CASCADE)
    guest_type = models.CharField(max_length=5, choices=GUEST_TYPES)

    class Meta:
        """Meta definition for Guest."""
    
        verbose_name = 'guest'
        verbose_name_plural = 'guests'

    def __str__(self):
        """Unicode representation of GUEST."""
        return self.guest.user.username
