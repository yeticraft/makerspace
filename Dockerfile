FROM python:latest as base

# Base environment config
ENV DEBIAN_FRONTEND noninteractive
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
# Prevent Python from writing the bytecode. Comment out for deployment
#ENV PYTHONDONTWRITEBYTECODE 1

# Set our working directory
WORKDIR /app

# Install our project dependencies:
RUN apt-get update && apt-get install -y \
        python-dev \
        libtiff5 \
        libfreetype6 \
        libjpeg-dev \
        zlib1g \
        libpq-dev postgresql postgresql-contrib
# For MySQL support, replace postgres lines with the following:
#        default-libmysqlclient-dev


######


FROM base as engine

#USER root
# simplistic (rigid) host file ownership solution for use during development
ARG UNAME=djangouser
ARG UID=1000
ARG GID=1000
RUN groupadd -g ${GID} -o ${UNAME}
RUN useradd -m -u ${UID} -g ${GID} -o -s /bin/bash ${UNAME}

# Default command after initialization is completed.
COPY runengine /usr/local/bin/

# Copy the requirements.txt
COPY app/requirements.txt /app/

# Ensure that all of the python modules are installed from the requirements file
RUN pip install -r requirements.txt


######


FROM engine as application

# Copy in any/all files from our application directory
COPY app/ /app
RUN chown -R ${UID}:${GID} /app

# Switch to the actual user before the final command
USER $UNAME

# Start the engine!
CMD ["runengine"]

# Make sure to create the project files before running this image on it's own!
# (see the Makefile's help on `make initialize`)